//Users
{
	"id": "user-01",
	"firstName": "Tricia", 
	"lastName":"Cruz", 
	"email":"tricia@email.com",
	"password":"tricia123",
	"isAdmin": true,
	"mobileNumber":"09123456789"
}

//Orders
{
	"id": "order-01",
	"user_id": "user-01",
	"transaction_date": "04/18/23",
	"status": "active",
	"total": 1
}

//Products
{
	"id": "prod-01",
	"name": "Sample Product",
	"description": "Sample product's description",
	"price": 500,
	"stocks": 10,
	"isActive": true
	"SKU": "ABC123"
}

//OrderProducts
{
	"id": "ordprod-01",
	"order_id": "order-01",  
	"product_id": "prod-01", 
	"quantity": 1,
	"Price": 500,
	"subTotal": 500
}

